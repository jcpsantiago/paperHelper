#' @title Standard error of the mean
#' @description Calculates de standard error of the mean for a set of values.
#' @param x numeric vector.
#' @return numeric value.
#' @details DETAILS
#' @examples
#' \dontrun{
#' if(interactive()){
#'  x <- rnorm(100)
#'  sem(x)
#'  }
#' }
#' @rdname sem
#' @export
#' @importFrom stats var
sem <- function(x){
  sqrt(stats::var(x)/length(x))
}

#' @title Calculate area under the curve (AUC)
#' @description FUNCTION_DESCRIPTION
#' @param order PARAM_DESCRIPTION
#' @param value PARAM_DESCRIPTION
#' @return OUTPUT_DESCRIPTION
#' @details DETAILS
#' @examples
#' \dontrun{
#' if(interactive()){
#'  #EXAMPLE1
#'  }
#' }
#' @seealso
#'  \code{\link[zoo]{rollmean}}
#' @rdname auc
#' @export
#' @importFrom zoo rollmean
auc <- function(order, value){

  sum(diff(order)*zoo::rollmean(value,2))
}

#' @title t.test shortcut
#' @description FUNCTION_DESCRIPTION
#' @param df PARAM_DESCRIPTION
#' @param value PARAM_DESCRIPTION
#' @param group PARAM_DESCRIPTION
#' @param ... PARAM_DESCRIPTION
#' @return OUTPUT_DESCRIPTION
#' @details DETAILS
#' @examples
#' \dontrun{
#' if(interactive()){
#'  #EXAMPLE1
#'  }
#' }
#' @seealso
#'  \code{\link[wrapr]{let}},\code{\link[wrapr]{mapsyms}}
#'  \code{\link[broom]{tidy}}
#'  \code{\link[stats]{t.test}}
#' @rdname run_ttest
#' @export
#' @importFrom wrapr let mapsyms
#' @importFrom broom tidy
#' @importFrom stats t.test
run_ttest <- function(df, value, group, ...){
  wrapr::let(wrapr::mapsyms(value,
                            group),
             broom::tidy(stats::t.test(value ~ group, data = df, ...)))
}
